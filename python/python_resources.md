# Distributions

- Anaconda
- pip3

# Python IDE

- Pyzo
- Spyder
- PyCharm
- Emacs/vim
- Atom

## Notebooks

- Jupyter


# Python Libraries

- Numpy: Linear algebra
- Matplotlib: Plotting
- Scipy: probability, statistics, advanced linear algebra
- Pandas: dataframes
- Scikits-learn (sklearn): machine learning
