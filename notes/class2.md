# Model-based Newsvendor 

We discussed the model based solution to the Newsvendor problem. The main idea is to first fit a distribution to the demand data, and then solve for the optimal solution of:
```math 
\max_q \mathbb{E}[-c q + p \min\{ q, Y\} ]~, 
```
where 

- $`q`$ is the order amount
- $`c`$ is the cost of the good
- $`p`$ is the sale price
- $`Y`$ is the random variable that represents the demand

The optimization problem can be solved by finding a $`q`$ that satisfies:
```math
\frac{d}{dq} \mathbb{E}[-c q + p \min\{ q, Y\} ] = 0 ~.
```
The value of $`q`$ that satisfies this equation can then be found by algebraic manipulation, rewriting the expectation as an integral, and using the [Leibniz integral rule](https://en.wikipedia.org/wiki/Leibniz_integral_rule).

The optimal order quantity $`q`$ is:
```math
q = F^{-1} \left( \frac{p-c}{p} \right)
```
$`F`$ is the [CDF](https://en.wikipedia.org/wiki/Cumulative_distribution_function) and $`F^{-1}`$ is its inverse, also known as the [quantile](https://en.wikipedia.org/wiki/Quantile_function).

## Python solution

The solution we generated in the class is [here](../solutions/newsvendor/modelbased.py)

## Simulator

A simple simulator can be found [here](http://rmdp.xyz:3838/cs980/newsvendor/). The code (in R) is [here](../code/FruitVendor).

